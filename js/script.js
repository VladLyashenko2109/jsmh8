// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
// Знайти елемент із id=”optionsList”. Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
//  Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// Встановіть в якості контента елемента з класом testParagraph наступний параграф – This is a paragraph
// Отримати елементи
// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


// 1

const p = document.body.querySelectorAll('p');
for(let node of p) {
    node.style.backgroundColor = '#ff0000'
}


// 2

const idOptionList = document.getElementById('optionsList');
console.log(idOptionList);
console.log(idOptionList.parentNode);
// console.log(idOptionList.childNodes);
let children = idOptionList.childNodes;
if(idOptionList.hasChildNodes){
     for(let nodes of children){
        console.log(nodes);
     }
}

// 3

const testParagraph = document.body.querySelector('#testParagraph');

testParagraph.innerHTML = 'This is a paragraph'

console.log(testParagraph.innerHTML);


// 4

const mainHeader = document.body.querySelector('.main-header').children;
// mainHeader.classList.add("nav-item")
for(let node of mainHeader) {
    node.classList.add("nav-item");
}

console.log(mainHeader);

const sectionTitle = document.body.querySelectorAll('.section-title');

for(let node of sectionTitle) {
    node.classList.remove('section-title')
}

console.log(sectionTitle);



// Опишіть своїми словами що таке Document Object Model (DOM)
//це html-документ, який представленний у виді дерева тегів
// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// перший показує текст одного елемента, а другий по усім
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Є методи пошуку за тегом, за классом, за айди, але самий кращий та найуніверсальніший - querySelectorAll